<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/list', 'CameraController@list')->name('list');
Route::get('/show/{camera}', 'CameraController@show')->name('show');
Route::resource('rent', 'RentController', ['only' => ['index','store']]);

Route::group(array('middleware' => 'role:' . \App\Models\User::ROLE_ADMIN), function () {
    Route::resource('camera', 'CameraController', ['except' => ['show']]);
});