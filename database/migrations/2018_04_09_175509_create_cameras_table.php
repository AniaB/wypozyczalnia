<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCamerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cameras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->text('mark');
            $table->text('model');
            $table->text('type');
            $table->decimal('resolution',6,2);
            $table->text('flash_lamp')->nullable();
            $table->integer('memory_cards');
            $table->text('communication')->nullable();
            $table->text('ISO');
            $table->decimal('price',6,2);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cameras');
    }
}
