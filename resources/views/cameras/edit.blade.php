@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <center>
                    <h2>@if(empty($camera->id)) Dodaj nowy aparat @else Edytuj aparat @endif </h2>
                </center>
            </div>
            <div class="panel-body">
                @if(\Illuminate\Support\Facades\Auth::user())
                    @if(empty($camera->id))
                        {{ Form::open(['route'=>'camera.store','class'=>'form-horizontal','files'=>'true']) }}
                    @else
                        {{ Form::open(['route' => ['camera.update',$camera->id],'class'=>'form-horizontal', 'files'=>'true', 'method'=>'PUT']) }}
                    @endif
                    {{ csrf_field() }}

                    {{ Form::label('image', 'Obraz', ['class'=>'control-label col-md-3']) }}
                    <div class="col-md-9">
                        {{ Form::file('image', ['style'=>'padding-bottom: 5px']) }}
                    </div>
                    @if ($errors->has('image'))
                        <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                    @endif

                    {{Form::label('mark', 'Marka aparatu', ['class'=>'control-label col-md-3'])}}
                    <div class="col-md-9" style="padding-bottom: 5px">
                        {{Form::text('mark', (empty($camera->id) ? null : $camera->mark), ['class'=>'form-control','id'=>'mark'])}}
                        @if ($errors->has('mark'))
                            <span class="help-block">
                            <strong>{{ $errors->first('mark') }}</strong>
                        </span>
                        @endif
                    </div>

                    {{Form::label('model', 'Model', ['class'=>'control-label col-md-3'])}}
                    <div class="col-md-9" style="padding-bottom: 5px">
                        {{Form::text('model', (empty($camera->id) ? null : $camera->model), ['class'=>'form-control','id'=>'model'])}}
                        @if ($errors->has('model'))
                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>

                    {{Form::label('type', 'Typ', ['class'=>'control-label col-md-3'])}}
                    <div class="col-md-9" style="padding-bottom: 5px">
                        {{Form::text('type', (empty($camera->id) ? null : $camera->type), ['class'=>'form-control','id'=>'type'])}}
                        @if ($errors->has('type'))
                            <span class="help-block">
                            <strong>{{ $errors->first('type') }}</strong>
                        </span>
                        @endif
                    </div>

                    {{Form::label('resolution', 'Rozdzielczość', ['class'=>'control-label col-md-3'])}}
                    <div class="col-md-9" style="padding-bottom: 5px">
                        {{Form::text('resolution', (empty($camera->id) ? null : $camera->resolution), ['class'=>'form-control','id'=>'resolution'])}}
                        @if ($errors->has('resolution'))
                            <span class="help-block">
                            <strong>{{ $errors->first('resolution') }}</strong>
                        </span>
                        @endif
                    </div>

                    {{Form::label('flash_lamp', 'Lampa_błyskowa', ['class'=>'control-label col-md-3'])}}
                    <div class="col-md-9" style="padding-bottom: 5px">
                        {{Form::text('flash_lamp', (empty($camera->id) ? null : $camera->flash_lamp), ['class'=>'form-control','id'=>'flash_lamp'])}}
                        @if ($errors->has('flash_lamp'))
                            <span class="help-block">
                            <strong>{{ $errors->first('flash_lamp') }}</strong>
                        </span>
                        @endif
                    </div>

                    {{Form::label('memory_cards', 'Pamięć', ['class'=>'control-label col-md-3'])}}
                    <div class="col-md-9" style="padding-bottom: 5px">
                        {{Form::number('memory_cards', (empty($camera->id) ? null : $camera->memory_cards), ['class'=>'form-control','id'=>'memory_cards'])}}
                        @if ($errors->has('memory_cards'))
                            <span class="help-block">
                            <strong>{{ $errors->first('memory_cards') }}</strong>
                        </span>
                        @endif
                    </div>

                    {{Form::label('communication', 'Komunikacja', ['class'=>'control-label col-md-3'])}}
                    <div class="col-md-9" style="padding-bottom: 5px">
                        {{Form::text('communication', (empty($camera->id) ? null : $camera->communication), ['class'=>'form-control','id'=>'communication'])}}
                        @if ($errors->has('communication'))
                            <span class="help-block">
                            <strong>{{ $errors->first('communication') }}</strong>
                        </span>
                        @endif
                    </div>

                    {{Form::label('ISO', 'Czułość ISO', ['class'=>'control-label col-md-3'])}}
                    <div class="col-md-9" style="padding-bottom: 5px">
                        {{Form::text('ISO', (empty($camera->id) ? null : $camera->ISO), ['class'=>'form-control','id'=>'ISO'])}}
                        @if ($errors->has('ISO'))
                            <span class="help-block">
                            <strong>{{ $errors->first('ISO') }}</strong>
                        </span>
                        @endif
                    </div>

                    {{Form::label('price', 'Cena wypożyczenia', ['class'=>'control-label col-md-3'])}}
                    <div class="col-md-9" style="padding-bottom: 5px">
                        {{Form::text('price', (empty($camera->id) ? null : $camera->price), ['class'=>'form-control','id'=>'price'])}}
                        @if ($errors->has('price'))
                            <span class="help-block">
                            <strong>{{ $errors->first('price') }}</strong>
                        </span>
                        @endif
                    </div>

                    {{Form::label('status', 'Dostępność', ['class'=>'control-label col-md-3'])}}
                    <div class="col-md-9" style="padding-bottom: 10px">
                        {{Form::select('status', ['1'=> 'dostępny', '0'=>'niedostępny'],empty($camera->id) ? 1 : $camera->status,['id'=>'status','class'=>'form-control'])}}
                        @if ($errors->has('status'))
                            <span class="help-block">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                        @endif
                    </div>

                    <br/>

                    <div class="form-group">
                    {{Form::submit('Zapisz',['class'=>'btn btn-primary'])}}
                    {{Html::link('camera','Cofnij',['class'=>'btn btn-default'])}}
                    </div>

                    {{ Form::close() }}

                @else
                    <p>Nie masz dostępu do tej strony!</p>
                @endif
            </div>
        </div>
    </div>



    <script src="{{ asset('js/edit_recipe.js') }}"></script>
@endsection

