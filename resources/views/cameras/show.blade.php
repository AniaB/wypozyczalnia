@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-info">
            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if(Auth::user())
                    <div class="panel panel-default">
                        <div class="row" style="padding-left: 30px; padding-right: 30px">
                            <div class="col-md-7 text-center">
                                <img src="/wypozyczalnia/storage/app/public/{{$camera->image}}" alt="Image" height="400"
                                     width="400">
                            </div>
                            <div class="col-md-5" style="padding-top: 40px">
                                <h2> {{ $camera->mark }} {{ $camera->model }} </h2>
                                <h4> Cena wypożyczenia za dobę: {{ $camera->price }}</h4>
                                <div class="row">
                                    <div class="col-md-12">
                                        {{ Form::open(['route'=>'rent.store','class'=>'form-horizontal']) }}
                                        {{ csrf_field() }}

                                        {{Form::label('count_day', 'Ilość dni', ['class'=>'control-label col-md-3'])}}
                                        <div class="col-md-9" style="padding-bottom: 5px">
                                            {{Form::number('count_day', null, ['class'=>'form-control','id'=>'count_day'])}}
                                            @if ($errors->has('count_day'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('count_day') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{--{{Form::label('price_all', 'Cena (całkowita)', ['class'=>'control-label col-md-3'])}}--}}
                                        {{--<div class="col-md-9" style="padding-bottom: 5px">--}}
                                            {{--{{Form::text('price_all', (empty($camera->id) ? null : $camera->price), ['class'=>'form-control','id'=>'price_all'])}}--}}
                                            {{--@if ($errors->has('price_all'))--}}
                                                {{--<span class="help-block">--}}
                                                    {{--<strong>{{ $errors->first('price_all') }}</strong>--}}
                                                {{--</span>--}}
                                            {{--@endif--}}
                                        {{--</div>--}}

                                        <br/>
                                        <br/>
                                        {{Form::submit('Wypożycz',['class'=>'btn btn-dark btn-block'])}}
                                    {{ Form::close() }}

                                </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h2> Parametry aparatu: </h2>
                        </div>
                        <div class="panel-body">
                            <h4><label>Marka: </label> {{ $camera->mark }}</h4>
                            <h4><label>Model aparatu: </label> {{ $camera->model }}</h4>
                            <h4><label>Typ aparatu: </label> {{ $camera->type }}</h4>
                            <h4><label>Rozdzielczość (Mpix): </label> {{ $camera->resolution }}</h4>
                            <h4><label>Lampa błyskowa: </label> {{ $camera->flash_lamp }}</h4>
                            <h4><label>Pamięć: </label> {{ $camera->memory_cards }}</h4>
                            <h4><label>Komunikacja: </label> {{ $camera->communication }}</h4>
                            <h4><label>Czułość ISO: </label> {{ $camera->ISO }}</h4>
                        </div>

                    </div>
                @else
                    <p>Nie masz dostępu do tej strony!</p>
                @endif
            </div>
        </div>
    </div>
@endsection
