@extends('layouts.app')

@section('content')


    <div class="container">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h2>Aparaty fotograficzne</h2>
            </div>
            <div class="panel-body">
                @if (Auth::user()->isAdmin())

                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <a href="{{ route('camera.create') }}" class="btn btn-primary">Dodaj aparat</a>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row">
                            <h5 class="col-md-2">Marka</h5>
                            <h5 class="col-md-2">Model</h5>
                            <h5 class="col-md-1">Typ</h5>
                            <h5 class="col-md-2">Czułość ISO</h5>
                            <h5 class="col-md-1">Cena wypożyczenia</h5>
                            <h5 class="col-md-1">Dostępność</h5>
                        </div>

                        @foreach ($cameras as $camera)
                            <div class="row">
                                <p class="col-md-2" style="padding-bottom: 5px">{{ $camera->mark }}</p>
                                <p class="col-md-2" style="padding-bottom: 5px">{{ $camera->model }}</p>
                                <p class="col-md-1" style="padding-bottom: 5px">{{ $camera->type }}</p>
                                <p class="col-md-2" style="padding-bottom: 5px">{{ $camera->ISO }}</p>
                                <p class="col-md-1" style="padding-bottom: 5px">{{ $camera->price }}</p>
                                <p class="col-md-1"
                                   style="padding-bottom: 5px">{{ $camera->status==1 ? 'dostępny' : 'niedostępny' }}</p>

                                <div class="col-md-2 text-right">
                                    <div class="col-md-6">
                                        <a href="{{ route('camera.edit', ['id' => $camera->id]) }}"
                                           class="btn btn-primary">{{ 'Edytuj' }}</a>
                                    </div>
                                    <div class="col-md-6">
                                        {{ Form::open(['route' => ['camera.destroy',$camera->id],'method'=>'DELETE']) }}
                                        {{ Form::submit('Usuń', ['class' => 'btn btn-danger']) }}
                                        {{Form::close()}}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                @else
                    <p>Nie masz dostępu do tej strony!</p>
                @endif
            </div>
        </div>
    </div>
    </div>


@endsection
