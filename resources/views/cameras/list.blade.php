@extends('layouts.app')

@section('content')


    <div class="container">
        @if (Session::has('camera_rents'))
            <div class="alert alert-success">{{ Session::get('camera_rents') }}</div>
        @endif
        <div class="panel panel-info">
            <div class="panel-heading">
                <h2>Aparaty fotograficzne</h2>
            </div>
            <div class="panel-body">
                @if (Auth::user())
                    <div class="container-fluid">

                        @foreach ($cameras as $camera)
                            <div class="col-md-3">
                                <div class="card">
                                    <a href="{{route('show',$camera->id)}}"><img class="card-img-top" src="./{{Storage::url($camera->image)}}" alt="Image" height="250" width="200"></a>
                                    <div class="card-body">
                                        <h4 class="card-title text-center">{{$camera->mark}} {{$camera->model}}</h4>
                                        <p class="card-text text-right">Cena: {{$camera->price}} zł</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                @else
                    <p>Nie masz dostępu do tej strony!</p>
                @endif
            </div>
        </div>
    </div>
    </div>


@endsection
