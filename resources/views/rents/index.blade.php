@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-info">
            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if(Auth::user())
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h2> Aparaty które wypożyczyłeś: </h2>
                        </div>
                        <div class="panel-body">
                            @foreach ($rents as $rent)
                                @if(!empty($rent->id))
                                <h3 class="bg-success text-white"
                                    style="height: 30px">{{ $rent->model }} {{ $rent->mark }}</h3>
                                <pre>
                                    <img class="text-center" src="/wypozyczalnia/storage/app/public/{{$rent->image}}"
                                         alt="Image" height="300" width="300">
                                    <div class="row">
                                        <p class="col-md-5 text-right">Ilość dni na ile wypożyczyłeś aparat:</p>
                                        <p class="col-md-5">{{ $rent->count_day }}</p>
                                    </div>
                                    <div class="row">
                                        <p class="col-md-5 text-right">Cena całkowita jaką zapłacisz przy oddawaniu aparatu:</p>
                                        <p class="col-md-5">{{ $rent->price_all }} zł</p>
                                    </div>
                                    <div class="row">
                                        <p class="col-md-5 text-right">Pamięć</p>
                                        <p class="col-md-5">{{ $rent->memory_cards }}</p>
                                    </div>
                                    <div class="row">
                                        <p class="col-md-5 text-right">Czułość ISO</p>
                                        <p class="col-md-5">{{ $rent->ISO }}</p>
                                    </div>
                                </pre>
                                @else
                                    <pre>
                                        <h2>Aktualnie nie masz wypożyczonego żadnego aparatu.</h2>
                                    </pre>
                                @endif
                            @endforeach
                        </div>

                    </div>
                @else
                    <p>Nie masz dostępu do tej strony!</p>
                @endif
            </div>
        </div>
    </div>
@endsection
