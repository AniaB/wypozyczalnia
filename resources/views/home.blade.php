@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="panel panel-success">
                    <div class="panel-heading"><h2>Wypożyczalnia aparatów fotograficznych</h2></div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @guest
                            <h4>Witamy w naszej wypożyczalni, aby kontynuować musisz się zalogować.</h4>
                            <h4>Jeśli nie posiadasz konta zachęcamy do rejestracji.</h4>
                        @else
                            @if (Auth::user()->isAdmin())
                                <h4> Jesteś adminem możesz zarządzać wypożyczalnią</h4>
                            @else
                                <h4>Witamy w naszej wypożyczalni.</h4>
                                <h4>Życzymy miłego pobytu na naszej stronie.</h4>
                            @endif
                        @endguest
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
