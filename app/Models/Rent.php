<?php

namespace App\Models;

use App\Models\Camera;
use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    protected $table = 'camera_rent';

    protected $fillable = [
        'count_day', 'price_all',
    ];

    public function cameras() {
        return $this->belongsTo(Camera::class);
    }
}
