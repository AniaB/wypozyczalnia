<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    const ROLE_ADMIN = 2;
    const ROLE_USER = 1;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Checks the admin
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role == User::ROLE_ADMIN;
    }

    /**
     * Check the role
     *
     * @param $role
     * @return bool
     */
    public function hasRole($role)
    {
        return $this->role == $role;
    }
}
