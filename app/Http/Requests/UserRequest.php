<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;


class UserRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * The validation rules to the request.
     *
     * @param $user
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        $rules = [
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($id)],
            'password' => 'required|min:6|confirmed',
        ];
        if ($this->method() == 'PUT') {
            $rules['password'] = 'nullable|min:6|confirmed';
        }

        return $rules;
    }

}
