<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CameraRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'mark' => 'required',
            'model' => 'required',
            'type' => 'required',
            'resolution' => 'required|regex:/^\d*(\.\d{1,2})?$/|max: 6',
            'memory_cards' => 'required|integer',
            'ISO' => 'required',
            'price' => 'required|regex:/^\d*(\.\d{1,2})?$/|max: 6',
        ];

        if ($this->file('image')) {
            $rules['image'] = 'required|image|mimes:jpeg,png,gif';
        }

        return $rules;
    }
}
