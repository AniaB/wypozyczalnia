<?php

namespace App\Http\Controllers;

use App\Http\Requests\RentRequest;
use App\Models\Camera;
use App\Models\Rent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class RentController extends Controller
{
    public function index()
    {
        $rents = DB::table('camera_rent')->leftJoin('cameras','camera_rent.camera_id','=','cameras.id')->where('user_id',Auth::user()->id)->get();
        return view('rents.index', compact('rents'));
    }

    public function store(RentRequest $request)
    {
        $rent = new Rent();
        $id = Session::get('id');
        $camera = Camera::find($id);
        $rent->fill($request->input());
        $rent->user_id = Auth::user()->id;
        $rent->camera_id = (int)$id;
        $rent->price_all = $camera->price * $rent->count_day;
        $camera->update(['status' => 0]);
        $rent->save();
        Session::flash('camera_rents','Zarezerwowałeś aparat, szczegóły zobaczysz na swoim profilu.');
        return Redirect::to('list');
    }

}
