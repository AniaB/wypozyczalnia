<?php

namespace App\Http\Controllers;

use App\Http\Requests\CameraRequest;
use App\Models\Camera;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class CameraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cameras = Camera::all();
        return view('cameras.index', compact('cameras'));
    }

    public function list()
    {
        $cameras = Camera::all()->where('status',1);
        return view('cameras.list', compact('cameras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cameras/edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CameraRequest $request, $id = null)
    {
        $camera = $id ? Camera::find($id) : new Camera();
        $camera->fill($request->input());
        if ($request->file('image')) {
            $camera->save();
            Storage::disk('public')->delete($camera->image);
            $camera->image = Storage::disk('public')->putFileAs('CamerasPhotos', $request->file('image'), $camera->id . '_image' . '.' . $request->file('image')->getClientOriginalExtension());
        }
        $camera->save();
        return Redirect::to('camera');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $camera = Camera::find($id);
        Session::put('id',$id);
        return view('cameras.show', compact('camera'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id=null)
    {
        $camera = Camera::find($id);
        if (empty($camera)) {
            return redirect()->action('CameraController@index');
        }
        return view('cameras/edit', compact('camera'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CameraRequest $request, $id)
    {
        return $this->store($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Camera::destroy($id);
        return back();
    }
}
